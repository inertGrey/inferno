# README #

Sophomore year project.

### What is this repository for? ###

This game is a 2D side scrolling platformer that is *very* loosely based on the literary work of fiction by Dante Alighieri called the Divine Comedy. More specifically Inferno.

The game is comprised of three levels. The first has a basic enemy, moving projectiles, and a pool of lava.

The second introduces a new type of flying enemies in addition to the ones introduced in level one. 

The third level contains platforming aspects along with a basic boss fight.

The player is transported to each new level when they touch the purple portal. If they touch the one at the end of the third level, they’re brought back to the main menu screen.


### How do I get set up? ###

Install java.

Run Inferno.jar.